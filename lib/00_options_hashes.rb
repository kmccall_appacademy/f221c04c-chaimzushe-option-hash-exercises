def transmogrify(string, options={})
  defaults = {
    times: 2,
    upcase: string.upcase,
    reverse: string.reverse
  }.merge(options)
  "#{string.upcase.reverse}" * defaults[:times]
 end
